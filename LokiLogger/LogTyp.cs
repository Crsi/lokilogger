namespace LokiLogger.Shared {
	public enum LogTyp
	{
		Normal,
		Exception,
		Return,
		Invoke,
		RestCall
	}
}